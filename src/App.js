import './App.css'
import { useEffect, useState } from 'react'
import API from './utils/API'
import ContactsList from './components/Contacts/ContactsList'
import CommentsList from './components/Comments/CommentsList'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faSpinner} from '@fortawesome/free-solid-svg-icons'


function App () {
  const [contacts, setContacts] = useState([])
  const [comments, setComments] = useState([])

  useEffect(() => {
    Promise.all([API.get('/users'), API.get('/comments')]).then(
      ([contactsResponse, commentsResponse]) => {
        setContacts(contactsResponse.data)
        setComments(commentsResponse.data)
      })

  }, [])


  if (contacts || comments) {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
              <ContactsList contacts={contacts} />
              <CommentsList comments={comments}/>
          </div>
        </div>
      </div>
    )
  } else {
    return <div>
      <FontAwesomeIcon icon={faSpinner} > Contacts Loading...</FontAwesomeIcon>
    </div>
  }
}

export default App
