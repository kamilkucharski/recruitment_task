import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import InfiniteScroll from 'react-infinite-scroll-component'

export default function CommentsList ({ comments }) {

  const [count, setCount] = useState({
    prev: 0,
    next: 10
  })
  const [hasMore, setHasMore] = useState(true)
  const [current, setCurrent] = useState(comments.slice(count.prev, count.next))

  if (comments) {

    const getMoreData = () => {
      if (current.length === comments.length) {
        setHasMore(false)
      }
      setTimeout(() => {
        setCurrent(current.concat(comments.slice(count.prev + 10, count.next + 10)))
      }, 100)
      setCount((prevState) => ({ prev: prevState.prev + 10, next: prevState.next + 10 }))
    }

    return (
      <div className="col">
        <div className="row justify-content-center pb-5">Comments</div>
        <InfiniteScroll
          next={getMoreData}
          hasMore={hasMore}
          loader={
            <FontAwesomeIcon
              icon={faSpinner}
              spin={true}>
              Contacts Loading...
            </FontAwesomeIcon>}
          dataLength={current.length}>
          <div>
            {current && current.map(((item, index) => (
              <div key={index} className="post">
                <div key={item.id}>
                  <label className="list-group-item" htmlFor={item.id}>
                    <label>Topic:</label> {item.name}
                    <p>{item.body}</p>
                  </label>
                </div>
              </div>
            )))}
          </div>
        </InfiniteScroll>
      </div>
    )
  } else {
    return <FontAwesomeIcon icon={faSpinner} spin={true}> Contacts Loading...</FontAwesomeIcon>
  }
}