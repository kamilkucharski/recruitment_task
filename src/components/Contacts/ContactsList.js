import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner, faUserCircle, faEnvelope } from '@fortawesome/free-solid-svg-icons'

export default function ContactsList ({ contacts }) {

  const [search, setSearch] = useState('')

  const filterContact = () => {
    if (search === '') {
      return contacts
    } else {
      return contacts.filter(item => item.name.startsWith(search)
        || item.username.startsWith(search))
    }
  }

  if (contacts) {

    return (
      <div className="col">
        <div className="row justify-content-center">Contacts</div>
        <div className="row my-2">
          <input type="text" value={search} onChange={e => setSearch(e.target.value)} placeholder="Search contact..."/>
        </div>
        <ul className="list-group">
          {filterContact().map(item => (
            <div key={item.id}>
              <div className="list-group-item" htmlFor={item.id}>
                <div className="row">
                  <div className="col-3 align-content-center">
                    <div>
                      <FontAwesomeIcon icon={faUserCircle} size="4x"/>
                    </div>
                  </div>
                  <div className="col-9">
                    <h5>{item.name}</h5>
                    <p style={{ color: 'grey' }}>{item.username}</p>
                    <FontAwesomeIcon icon={faEnvelope}/>{item.website}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </ul>
      </div>
    )
  } else {
    return <FontAwesomeIcon icon={faSpinner} spin={true}> Contacts Loading...</FontAwesomeIcon>
  }
}